#include <stdio.h>
#include <iostream>
#include <string.h>
#include <cstdlib>
#include <cstring>
#include <stdlib.h>
#include <fstream>
#include <string.h>
#include "Classes.h"
#include <malloc.h>

using namespace std;

const int L = 1000;

void reverse(char s[])
 {
     int i, j;
     char c;
 
     for (i = 0, j = strlen(s)-1; i<j; i++, j--) {
         c = s[i];
         s[i] = s[j];
         s[j] = c;
     }
 }

void myitoa(int n, char s[])
 {
     int i, sign;
 
     if ((sign = n) < 0)  /* записываем знак */
         n = -n;          /* делаем n положительным числом */
     i = 0;
     do {       /* генерируем цифры в обратном порядке */
         s[i++] = n % 10 + '0';   /* берем следующую цифру */
     } while ((n /= 10) > 0);     /* удаляем */
     if (sign < 0)
         s[i++] = '-';
     s[i] = '\0';
     reverse(s);
 }

Table::Table(char* name, unsigned int lines, unsigned int coloumns)
{
	this->name = (char*)malloc(sizeof(char) * strlen(name) + sizeof(char));
	memcpy(this->name, name, strlen(name) + sizeof(char));

	this->lines = lines;
	this->real_lines = 0;
	this->coloumns = coloumns;

	this->maintable = (int**)malloc(sizeof(int*) * lines);
	/*for(int i = 0; i<lines; i++)
		this->maintable[i]  = (int*)malloc(sizeof(int) * coloumns);*/

	this->posts = (post*)malloc(sizeof(post) * L);
	this->textdata = (char*)malloc(sizeof(char) * L);
	textdata[0] = '\0';

	this->n_maxposts = L;
	this->n_maxtextdata = L;
	this->n_posts = 0;
	this->n_textdata = 0;
}

void Table::SetOtitles(oTitle* headers)
{
	this->headers = (oTitle*) malloc(sizeof(oTitle) * coloumns);
	for(int i = 0; i<coloumns; i++)
	{
		this->headers[i].title = (char*)malloc(strlen(headers[i].title) * sizeof(char) + sizeof(char)); 
		memcpy(this->headers[i].title, headers[i].title, strlen(headers[i].title) * sizeof(char) + sizeof(char));
		this->headers[i].type = headers[i].type;
	
	}
}

void Table::AddLine(int* line, post* postdata, char* textdata, int posts)
{
	int addoffset = strlen(this->textdata);
	int addoffsetline = this->n_posts;
	if(this->n_textdata + strlen(textdata) >= this->n_maxtextdata)
	{
		this->textdata = (char*)realloc(this->textdata, this->n_maxtextdata * L);
		this->n_maxtextdata *= L;
	}
	if(this->n_posts + posts >= this->n_maxposts)
	{
		//
		this->posts = (post*)realloc(this->posts, this->n_maxposts * L);
		this->n_maxposts *= L;
	}
	strcat(this->textdata, textdata);
	this->n_textdata = strlen(this->textdata);
		
	for(int i = 0; i < posts; i++)
	{
		this->posts[i+this->n_posts].deleted = postdata[i].deleted;
		this->posts[i+this->n_posts].hash = postdata[i].hash;
		this->posts[i+this->n_posts].length = postdata[i].length;
		this->posts[i+this->n_posts].offset = postdata[i].offset + addoffset;
	}
	this->n_posts += posts;

	if(this->lines == this->real_lines)
	{
		int** old = this->maintable;
		this->maintable = (int**)malloc(sizeof(int*) * this->real_lines + 1);
		for(int i = 0; i<real_lines; i++)
		{
			this->maintable[i]  = (int*)malloc(sizeof(int) * coloumns);
			for(int j = 0; j<this->coloumns; j++)
				this->maintable[i][j] = old[i][j] ;
		}
		this->lines++;
	}

	this->maintable[this->real_lines] = (int*)malloc(sizeof(int) * coloumns);

	for(int i = 0; i<this->coloumns; i++)
	{
		if(headers[i].type == 2)
			line[i]+=addoffsetline;
		this->maintable[this->real_lines][i] = line[i];
	}
	this->real_lines++;
}

char* Table::Name()
{
	return this->name;
}

int Table::Lines()
{
	return this->lines;
}

int Table::Coloumns()
{
	return this->coloumns;
}

oTitle*	Table::Headers()
{
	return this->headers;
}

int** Table::Maintable()
{
	return this->maintable;
}

post* Table::Posts()
{
	return this->posts;
}

char* Table::TextData()
{
	return this->textdata;
}

DataBase::DataBase()
{}

DataBase::DataBase(char* name)
{
	this->name = (char*)malloc(sizeof(char) * strlen(name)+sizeof(char));
	memcpy(this->name, name, strlen(name)+sizeof(char));
	tables = (Table*)malloc(sizeof(Table) * L);
	this->count = 0;
	this->countmax = L;
}
     	
char* DataBase::Name()
{
	return this->name;
}

int DataBase::Count()
{
	return this->count;
}

Table* DataBase::Tables()
{
	return this->tables;
}
     	
void DataBase::AddTable(Table t)
{
	if(count == countmax)
	{
		tables = (Table*)realloc(tables, count * 2);
		this->countmax *= 2;
	}
	tables[count] = t;
	count++;
}

void DataBase::DelTable(char* name)
{
	for(int i = 0; i<this->count; i++)
		if(strcmp(this->tables[i].Name() , name) == 0)
		{
			int j;
			for(j = i; j < this->count - 1; j++)
				this->tables[j] = this->tables[j+1];
			this->count--;
			return;
			//
			//free(&(this->tables[j + 1]));
		}
}

const int H = 10;
const int MULT = 31;

int Table::myhash(char* line)
{
	unsigned int h = 0;
    unsigned char* p;
 
    for (p = (unsigned char*) line; *p != '\0'; p++) {
        h = MULT * h + (*p);
    }
    return h % H;
}

int Operation(condtype c, char* line1, char* line2, datatype dt)
{
	int s;
	if(dt == (datatype)1)
	{
		int a = atoi(line1);
		int b = atoi(line2);
		s = a - b;
	}
	else s = strcmp(line1, line2);
	if((c == 1 || c == 6 || c == 4) && s == 0)
		return 1;
	if((c== 3 || c == 4) && s > 0)
		return 1;
	if((c == 5 || c == 6) && s < 0)
		return 1;
	if(c == 2 && s != 0)
		return 1;
	return 0;
}

void Table::ChangeLine(Condition c, char* title, char* data)
{
	int cn = -1;
	for(int i = 0; i < this->coloumns; i++)
		if(strcmp(this->headers[i].title, c.title) == 0)
		{
			cn = i;
			break;
		}

	int cn1 = -1;
	for(int j = 0; j < this->coloumns; j++)
		if(strcmp(this->headers[j].title, title) == 0)
		{
			cn1 = j;
			break;
		}
	if(headers[cn].type != c.dt)
		throw new exception;
	
	char* buf = (char*)malloc(sizeof(char) * L);
	for(int i = 0; i <this->lines; i++)
	{
		if(c.dt == (datatype)1)
		{
			myitoa(this->maintable[i][cn], buf);
		}
		else 
		{
			memcpy(buf, this->textdata + posts[this->maintable[i][cn]].offset, posts[this->maintable[i][cn]].length);
			buf[posts[this->maintable[i][cn]].length] = '\0';
		}
		if(Operation(c.ct, buf, c.data, c.dt) == 1)
		{
			if(headers[cn1].type == 1)
			{
				this->maintable[i][cn1] = atoi(data);
			}
			else if(headers[cn1].type == 2)
			{
				posts[this->maintable[i][cn1]].deleted = 1;
				if(this->n_posts + 1 >= this->n_maxposts)
				{
					//
					this->posts = (post*)realloc(this->posts, this->n_maxposts * L);
					this->n_maxposts *= L;
				}
				int addoffset = n_textdata;

				strcat(this->textdata, data);
				this->n_textdata = strlen(this->textdata);
		
				this->posts[this->n_posts].deleted = 0;
				this->posts[this->n_posts].hash = myhash(data);
				this->posts[this->n_posts].length = strlen(data);
				this->posts[this->n_posts].offset = addoffset;
				maintable[i][cn1] = n_posts;
				this->n_posts++;
				
			}
		}
			
	}

}

void Table::DelLine(Condition c)
{
	int cn = -1;
	for(int i = 0; i < this->coloumns; i++)
		if(strcmp(this->headers[i].title, c.title) == 0)
		{
			cn = i;
			break;
		}
	if(headers[cn].type != c.dt)
		throw new exception;

	char* buf = (char*)malloc(sizeof(char) * L);
	for(int i = 0; i <this->lines; i++)
	{
		if(c.dt == (datatype)1)
		{
			myitoa(this->maintable[i][cn], buf);
		}
		else 
		{
			memcpy(buf, this->textdata + posts[this->maintable[i][cn]].offset, posts[this->maintable[i][cn]].length);
			buf[posts[this->maintable[i][cn]].length] = '\0';
		}
		if(Operation(c.ct, buf, c.data, c.dt) == 1)
		{
			for(int j = 0; j< this->coloumns; j++)
				if(this->headers[i].type == 2)
					posts[this->maintable[i][j]].deleted = 1;
			for(int j = i; j < this->lines - 1; j++)
				this->maintable[j] = this->maintable[j+1];
			this->lines--;
			this->real_lines--;
		}
	}

}

