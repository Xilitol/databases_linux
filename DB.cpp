#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <string.h>
#include "Classes.h"
#include <stdio.h>
#include <stdlib.h>

using namespace std;

//Max length of line
const int L = 300;
//
const int P = 10;

//
const int H = 10;
const int MULT = 31;

DataBase db;

int myhash(char* line)
{
	unsigned int h = 0;
    unsigned char* p;
 
    for (p = (unsigned char*) line; *p != '\0'; p++) {
        h = MULT * h + (*p);
    }
    return h % H;
}

void mreverse(char s[])
{
     int i, j;
     char c;
 
     for (i = 0, j = strlen(s)-1; i<j; i++, j--) {
         c = s[i];
         s[i] = s[j];
         s[j] = c;
     }
}

void mmyitoa(int n, char s[])
{
     int i, sign;
 
     if ((sign = n) < 0)  /* записываем знак */
         n = -n;          /* делаем n положительным числом */
     i = 0;
     do {       /* генерируем цифры в обратном порядке */
         s[i++] = n % 10 + '0';   /* берем следующую цифру */
     } while ((n /= 10) > 0);     /* удаляем */
     if (sign < 0)
         s[i++] = '-';
     s[i] = '\0';
     mreverse(s);
}

//Input
oTitle OneTitle(char* line)
{
	struct oTitle s0;
	char buf[L];
	int i;
	for(i = 0; i < strlen(line); i++)
	{
		if(line[i] != ':')
			buf[i] = line[i];	
		else 
		{
			buf[i] = '\0';
			break;
		}
	}
	s0.title = (char*) malloc(sizeof(char) * strlen(buf) + sizeof(char));
	memcpy(s0.title, buf, strlen(buf) + sizeof(char));
	i++;
	int k;
	for(k = i; i<strlen(line); i++)
		buf[i-k] = line[i];
	buf[i-k] = '\0';
	s0.type = (datatype)atoi(buf);
	return s0;
}

//Input
oTitle* MakeLine(char* line, int N)
{
	oTitle* titles = (oTitle*)malloc(sizeof(oTitle) * N);
	char arr[L];
	for(int i = 0, j = -1, k = 0;i<= strlen(line); i++)
	{
		if(line[i] != ',' && line[i] != '\0')
			arr[i-j-1] = line[i];
		else
		{
			arr[i-j-1] = '\0';
			titles[k] = OneTitle(arr);
			j = i;
			k++;
		}
	}
	return titles;
}

//Input
void MakeOnePost(char* arr, oTitle* titles, int NumberOfColomn, int* NumberOfTextPosts, char datatext[], int* data, post* postdata)
{
	if(titles[NumberOfColomn].type == 1)
		(data[NumberOfColomn]) = atoi(arr);
	else if(titles[NumberOfColomn].type == 2)
	{
		(*NumberOfTextPosts)++;
		
		postdata = (post*)realloc(postdata, sizeof(post) * (*NumberOfTextPosts));;
		
		int yu = strlen(datatext);
		int i;

		for(i = yu; i<yu + strlen(arr); i++)
			datatext[i] = arr[i - yu];
		
		datatext[i] = '\0';

		post p;
		p.deleted = 0;
		p.hash = myhash(arr);
		p.length = strlen(arr);
		p.offset = yu;
		postdata[*NumberOfTextPosts - 1] = p;
		data[NumberOfColomn] = *NumberOfTextPosts - 1;
	}
}

//Input
int MakeOneLine(char* line, oTitle* titles, int N, char* datatext, int* data, post* postdata)
{
	char arr[L];
	int l = 0;
	for(int i = 0, j = -1, k = 0; i<= strlen(line); i++)
	{
		if(line[i] != ',' && line[i] != '\0' && line[i] != '\n' && line[i] != '\a')
			arr[i-j-1] = line[i];
		else
		{
			arr[i-j-1] = '\0';
			MakeOnePost(arr, titles, k, &l, datatext, data, postdata);
			j = i;
			k++;
		}
	}
	return l;
}

//This function will return DataBase type
void input(char* filename)
{
	db = DataBase();
	char buff[L];

	FILE *f;
	f = fopen(filename, "r");
	try
	{
		fgets(buff, L, f);

		int zap = 0;
		for(int i = 0; i < strlen(buff); i++)
			if(buff[i] == ',')
			{
				zap = i;
				break;
			}	
		if(zap == 0)
			throw new exception;

		char name[L];
		strncpy(name, buff, zap);
		name[zap] = '\0';

		db = DataBase(name);

		char count_s[L];
		for(int i = zap; i<strlen(buff); i++)
			count_s[i-zap]=buff[i+1];
		int N_tables = atoi(count_s);

		for(int i = 0; i < N_tables; i++)
		{
			fgets(buff, L, f);

			int zap = 0;
			for(int j = 0; j < strlen(buff); j++)
				if(buff[j] == ',')
				{
					zap = j;
					break;
				}
			if(zap == 0)
				throw new exception;

			char tname[L];
			strncpy(tname, buff, zap);
			tname[zap] = '\0';

			int zap2 = 0;
			for(int j = zap + 1; j<strlen(buff); j++)
				if(buff[j] == ',')
				{
					zap2 = j;
					break;
				}
			if(zap2 == 0)
				throw new exception;

			char count_s2[L];
			for(int j = zap+1; j<zap2; j++)
				count_s2[j-zap-1] = buff[j];
			count_s2[zap2-zap-1] = '\0';
			int colomns = atoi(count_s2);

			char count_s3[L];
			for(int j = zap2; j<strlen(buff); j++)
				count_s3[j-zap2]=buff[j+1];
			int lines = atoi(count_s3);

			Table t = Table(tname, lines, colomns);

			fgets(buff, L, f);
			
			oTitle* titles = MakeLine(buff, colomns);
			t.SetOtitles(titles);

			for(int j = 0; j<lines; j++)
			{
				char datatext[L];
				datatext[0] = '\0';
				int* data = (int*)malloc(sizeof(int)*colomns);
				post* postdata = (post*)malloc(0);
				fgets(buff, L, f);
				int posts = MakeOneLine(buff, titles, colomns, datatext, data, postdata);
				t.AddLine(data, postdata, datatext, posts);
			}
			db.AddTable(t);
		}
		fclose(f);
	}
	catch(exception e)
	{
		printf("Error reading DB\n");
	}
}

//Output
void output(char* filename)
{
	char buff[L], buff1[L];

	try
	{
		FILE *f;
		f = fopen(filename, "w");

		memcpy(buff, db.Name(), strlen(db.Name()) + 1);
		strcat(buff, ",");
		
		mmyitoa(db.Count(), buff1);
		
		strcat(buff, buff1);
		strcat(buff, "\n");
		fwrite(buff, sizeof(char), strlen(buff), f);

		for(int i = 0; i<db.Count(); i++)
		{
			memcpy(buff, db.Tables()[i].Name(), strlen(db.Tables()[i].Name()) + 1);
			strcat(buff, ",");
			
			mmyitoa(db.Tables()[i].Coloumns(), buff1);
			strcat(buff, buff1);
			strcat(buff, ",");
			mmyitoa(db.Tables()[i].Lines(), buff1);
			
			strcat(buff, buff1);
			strcat(buff, "\n");
			fwrite(buff, sizeof(char), strlen(buff), f);

			buff[0] = '\0';
			for(int j = 0; j<db.Tables()[i].Coloumns(); j++)
			{
				strcat(buff, db.Tables()[i].Headers()[j].title);
				strcat(buff, ":");
				int y = db.Tables()[i].Headers()[j].type;
				mmyitoa(y, buff1);
					
				strcat(buff, buff1);
				if(j != db.Tables()[i].Coloumns() - 1)
					strcat(buff, ",");
				else strcat(buff, "\n");
			}
			fwrite(buff, sizeof(char), strlen(buff), f);

			int j;
			for(j = 0; j<db.Tables()[i].Lines(); j++)
			{
				buff[0] = '\0';
				for(int k = 0; k<db.Tables()[i].Coloumns(); k++)
				{
					if(db.Tables()[i].Headers()[k].type == 1)
					{
						mmyitoa(db.Tables()[i].Maintable()[j][k], buff1);
							
					}
					else 	
					{
						int index = db.Tables()[i].Maintable()[j][k];
						post p = db.Tables()[i].Posts()[index];
						memcpy(buff1, db.Tables()[i].TextData() + p.offset, p.length);
						buff1[p.length]='\0';
					}
					strcat(buff, buff1);
					if(k != db.Tables()[i].Coloumns() - 1)
						strcat(buff, ",");
				}
				if(!(i == db.Count() - 1 && j == db.Tables()[i].Lines() - 1))
					strcat(buff, "\n");
				fwrite(buff, sizeof(char), strlen(buff), f);
			}
		}

		fclose(f);
	}
	catch(exception e)
	{
		printf("Error writing\n");
	}

}

//
//
//
void GetTwoLines(char* line, char* name, char symbol)
{
	char *k = strchr(line, symbol);
	int probel;
	if(k != 0)
		probel = k-line;
	else probel = strlen(line);

	memcpy(name, line, probel);
	name[probel] = '\0';

	char line2[L];

	memcpy(line, line + strlen(name) + 1, strlen(line) - strlen(name));
}

 
void GetCondition(char* line, Condition* c)
{
	char* eq = strchr(line, '=');
	char* more = strchr(line, '>');
	char* less = strchr(line, '<');
	char* f = 0x00, f1 = 0x00;
	int l = 0;

	if(eq != 0x00)
	{
		if(more == 0x00 && less == 0x00)
		{
			char* neq = strchr(line, '!');
			if(neq != 0x00)
			{
				c->ct = (condtype)2;
				f = neq;
				l = 2;
			}
			else 
			{
				c->ct = (condtype)1;
				f = eq;
				l = 1;
			}
		}
		else 
			if(more == eq - sizeof(char))
			{
				c->ct = (condtype)4;
				f = more;
				l = 2;
			}
			else
				if(less == eq - sizeof(char))
				{
					c->ct = (condtype)6;
					f = less;
					l = 2;
				}
	}
	else 
		if(more == 0x00)
		{
			c->ct = (condtype)5;
			f = less;
			l = 1;
		}
		else 
			if(less == 0x00)
			{
				c->ct = (condtype)3;
				f = more;
				l = 1;
			}
	if(c->ct == 0) throw new exception;


	memcpy(c->title, line, f-line);
	c->title[f-line] = '\0';

	if(strchr(f, '\'') == 0x00 )
	{
		memcpy(c->data, f+l, strlen(f) - l + 1);
		c->dt = (datatype)1;
	}
	else
	{
		memcpy(c->data, f + l + 1, strlen(f) - l - 2);
		c->data[strlen(f) - l - 2] = '\0';
		c->dt = (datatype)2;
	}

	if(c->ct == (condtype)0 || c->dt == (datatype)0)
		throw new exception;
}

//
void cmd_crt(char* line)
{
	char* name = (char*) malloc(L * sizeof(char));
	GetTwoLines(line, name, ' ');
	
	int z = 0;
	for(int i = 0; i < strlen(line); i++)
		if(line[i] == ',')
			z++;
	if(z == 0)
		throw new exception;

	Table t = Table(name, 0, z + 1);

	oTitle* titles = MakeLine(line, z + 1);
	t.SetOtitles(titles);
	
	db.AddTable(t);
}

//
void cmd_ins(char* line)
{
	char* name = (char*) malloc(L * sizeof(char));
	GetTwoLines(line, name, ' ');

	int k = -1;
	for (int i = 0; i<db.Count(); i++)
	{
		if(strcmp(db.Tables()[i].Name(), name) == 0)
			k = i;
	}
	if(k == -1)
		throw new exception;

	char datatext[L];
	datatext[0] = '\0';
	int* data = (int*)malloc(sizeof(int)*db.Tables()[k].Coloumns());
	post* postdata = (post*)malloc(0);
	int posts = MakeOneLine(line, db.Tables()[k].Headers(), db.Tables()[k].Coloumns(), datatext, data, postdata);
	db.Tables()[k].AddLine(data, postdata, datatext, posts);
}

//
void cmd_upd(char* line)
{
	char* name = (char*) malloc(L * sizeof(char));
	GetTwoLines(line, name, ' ');
	char* cond = (char*) malloc(L * sizeof(char));
	GetTwoLines(line, cond, ' ');

	Condition c;

	c.title = (char*) malloc(L * sizeof(char));
	c.ct = (condtype) 0;
	c.dt = (datatype) 0;
	c.data = (char*) malloc(L * sizeof(char));

	GetCondition(cond, &c);

	char* title = (char*) malloc(L * sizeof(char));
	char* data = (char*) malloc(L * sizeof(char));
	GetTwoLines(line, title, '=');

	if(strchr(line, '\'') != 0x00)
	{
		memcpy(data, line + sizeof('\''), strlen(line) - 2 * sizeof('\''));
		data[strlen(line) - 2 * sizeof('\'')] = '\0';
	}
	else memcpy(data, line, strlen(line) + sizeof('\0'));

	int k = -1;
	for (int i = 0; i<db.Count(); i++)
	{
		if(strcmp(db.Tables()[i].Name(), name) == 0)
			k = i;
	}
	if(k == -1)
		throw new exception;

	db.Tables()[k].ChangeLine(c, title, data);

	int y = 0;
}

//
void cmd_det(char* line)
{
	char* name = (char*) malloc(L * sizeof(char));
	GetTwoLines(line, name, ' ');
	db.DelTable(line);
}

//
void cmd_del(char* line)
{
	char* name = (char*) malloc(L * sizeof(char));
	GetTwoLines(line, name, ' ');

	Condition c;

	c.title = (char*) malloc(L * sizeof(char));
	c.ct = (condtype) 0;
	c.dt = (datatype) 0;
	c.data = (char*) malloc(L * sizeof(char));

	GetCondition(line, &c);

	int k = -1;
	for (int i = 0; i<db.Count(); i++)
	{
		if(strcmp(db.Tables()[i].Name(), name) == 0)
			k = i;
	}
	if(k == -1)
		throw new exception;

	db.Tables()[k].DelLine(c);
}

//
//
//y - 
void cmd_sel(char* line, int y)
{
	char* name = (char*) malloc(L * sizeof(char));
	GetTwoLines(line, name, ' ');
	int Z = 0;
}

void command2(char* line, int y)
{
	if(line[strlen(line) - 1] == '\n')
		line[strlen(line) - 1] = '\0';
	if(strchr(line, ' ') - line !=3)
		throw new exception;
	const int d = 4;
	char cmdname[d];
	memcpy(cmdname, line, d - 1);
	cmdname[d-1] = '\0';
	
	char cmddata[L];
	memcpy(cmddata, line + d, strlen(line) - d);
	cmddata[strlen(line) - d ] = '\0';

	if(cmdname[0] == 'C' && cmdname[1] == 'R'&& cmdname[2] == 'T')
		cmd_crt(cmddata);
	else 
		if(cmdname[0] == 'I' && cmdname[1] == 'N'&& cmdname[2] == 'S')
			cmd_ins(cmddata);
		else
			if(cmdname[0] == 'U' && cmdname[1] == 'P'&& cmdname[2] == 'D')
				cmd_upd(cmddata);
			else
				if(cmdname[0] == 'D' && cmdname[1] == 'E'&& cmdname[2] == 'T')
					cmd_det(cmddata);
				else
					if(cmdname[0] == 'D' && cmdname[1] == 'E'&& cmdname[2] == 'L')
						cmd_del(cmddata);
					else
						if(cmdname[0] == 'S' && cmdname[1] == 'E'&& cmdname[2] == 'L')
							cmd_sel(cmddata, y);
						else throw new exception;

}
void command(char* filename)
{
	char buff[L];

	try
	{
		FILE *f;
		int y = 1; //
		f = fopen(filename, "r");
		while(!feof(f))
		{
			fgets(buff, L, f);
			command2(buff, y);
		}
	}
	catch (exception e)
	{
		printf("Error reading commands\n");
	}
}

int main(int argc, char* argv[])
{
	input(argv[1]);
	command(argv[2]);
	output(argv[3]);
	
	getchar();
    return 0;
}
